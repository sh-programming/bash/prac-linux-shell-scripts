echo "PROGRAM TO PERFORM SELECTION SORT"

set -A arr 6 8 9 7 4 2 1 3

echo "ORIGINAL ARRAY:"
for (( j = 0; j < 8; j++ )) do
	echo  ${arr[j]}
	done
for (( i = 0; i < 8; i++ )) do
	min=${arr[$i]}
	index=$i
	for (( j = i; j < 8; j++ )) do
		if [ ${arr[j]} -lt $min ]
		then
			min=${arr[j]}
			index=$j
		fi
	done
	temp=${arr[i]}
	arr[i]=${arr[index]}
	arr[index]=$temp
done
echo "SORTED ARRAY:"
for (( j = 0; j < 8; j++ )) do
	echo  ${arr[j]}
done
